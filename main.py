#!/usr/bin/env python3

import requests
import json
from urllib.parse import quote_plus
import tkinter as tk
from tkinter import ttk
import os
import os.path
from urllib.parse import quote_plus, quote

snippets_file_path = "snippets.json"

snippets = list()

def tk_find_all(text, pattern):
    if pattern == "":
        return list()
    matches = list()
    index = "1.0"
    while True:
        position = text.search(pattern, index, nocase=True, stopindex=tk.END)
        if position == "":
            break
        matches.append((position, f"{position}+{len(pattern)}c"))
        x, y = map(int, position.split("."))
        index = f"{x}.{y+len(pattern)}"
        pass
    return matches

gitlab_token = os.environ.get("GITLAB_TOKEN", None)
gitlab_url = os.environ.get("GITLAB_URL", "https://gitlab.com")
gitlab_project = os.environ.get("GITLAB_PROJECT", None)

def premade_request():
    if gitlab_token in ["", None]:
        print("error, token not set")
        return None
    local_gitlab_url = gitlab_url
    if local_gitlab_url[-1] != "/":
        local_gitlab_url += "/"
        pass
    local_gitlab_url += "api/v4/"
    if gitlab_project not in ["", None]:
        quoted = quote_plus(gitlab_project)
        local_gitlab_url += f"projects/{quoted}/"
        pass
    gitlab_headers = {
        "PRIVATE-TOKEN": gitlab_token,
        "Content-Type": "application/json",
    }
    return (local_gitlab_url, gitlab_headers)

def dump_snippets():
    with open(snippets_file_path, "w") as f:
        json.dump(snippets, f)
        pass
    pass

def update_snippet_content(snippet, sess):
    local_gitlab_url, gitlab_headers = premade_request()
    snippet["content"] = dict()
    for sfile in snippet["files"]:
        file_path = quote(sfile["path"])
        path = f"snippets/{snippet['id']}/files/main/{file_path}/raw"
        snippet["content"][sfile["path"]] = sess.get(local_gitlab_url + path).text
        pass
    pass

def update_snippets():
    global snippets
    snippets = list()
    local_gitlab_url, gitlab_headers = premade_request()
    with requests.Session() as sess:
        sess.headers = gitlab_headers
        page = 1
        while True:
            print(f"Downloading page {page}...")
            snippets_resp = sess.get(local_gitlab_url + f"snippets?per_page=100&page={page}")
            snippets_segment = snippets_resp.json()
            if len(snippets_segment) == 0:
                break
            snippets += snippets_segment
            page += 1
            pass
        print("Pages downloaded")
        for snippet in snippets:
            print(f"Downloading snippet {snippet['id']}...")
            update_snippet_content(snippet, sess)
            pass
        pass
    dump_snippets()
    return True

def update_command():
    update_snippets()
    update_search()
    pass

if os.path.isfile(snippets_file_path):
    with open(snippets_file_path, "r") as f:
        snippets = json.load(f)
        pass
    pass

def open_config():
    conf = tk.Toplevel(window)
    token = tk.StringVar(value=gitlab_token)
    token_entry = tk.Entry(conf, textvariable=token)
    token_label = tk.Label(conf, text="Token:")
    project = tk.StringVar(value=gitlab_project)
    project_entry = tk.Entry(conf, textvariable=project)
    project_label = tk.Label(conf, text="Project:")
    url = tk.StringVar(value=gitlab_url)
    url_entry = tk.Entry(conf, textvariable=url)
    url_label = tk.Label(conf, text="Url:")
    def apply_settings():
        global gitlab_token, gitlab_project, gitlab_url
        gitlab_token = token.get()
        gitlab_project = project.get()
        gitlab_url = url.get()
        conf.destroy()
        conf.update()
        pass
    update = tk.Button(conf, text="Apply settings", command=apply_settings)
    token_label.grid(row=0, column=0)
    project_label.grid(row=1, column=0)
    url_label.grid(row=2, column=0)
    token_entry.grid(row=0, column=1)
    project_entry.grid(row=1, column=1)
    url_entry.grid(row=2, column=1)
    update.grid(row=3, column=0, columnspan=2)
    pass

def delete_snippet_selected():
    if last_selected_element == None:
        return
    delete_snippet(visible_snippets[last_selected_element[0]])
    pass

window = tk.Tk()
visible_snippets = list()
sv = tk.StringVar()
lv = tk.StringVar()
sn_name = tk.StringVar()
lb = tk.Listbox(window, listvariable=lv)
sn_desc = tk.StringVar()
sn_name_label = tk.Label(window, textvariable=sn_name)
sn_desc_label = tk.Label(window, textvariable=sn_desc)
search_entry = tk.Entry(window, textvariable=sv)
tabs = ttk.Notebook(window, height=0)
menubar = tk.Menu(window)
menubar.add_command(label="Configure", command=open_config)
menubar.add_command(label="Update", command=update_command)
code_elems = list()
snippets_btns = tk.Frame(window)
new_snippet_name = tk.StringVar()
new_snippet_entry = tk.Entry(snippets_btns, textvariable=new_snippet_name)
delete_snippet_btn = tk.Button(snippets_btns, text="Delete snippet", command=
                               delete_snippet_selected)
new_snippet_btn = tk.Button(snippets_btns, text="New snippet", command=
                            lambda n=new_snippet_name: create_snippet(n.get()))

def update_search(name=None, index=None, mode=None):
    global visible_snippets
    search = sv.get()
    lb.delete(0, tk.END)
    visible_snippets = list()
    for snippet in sorted(snippets, key=lambda x: x["id"]):
        if "content" not in snippet:
            print("Snippets are corrupted, please, update your cache")
            return
        content = "\n".join(snippet["content"].values())
        if (search.lower() in snippet["title"].lower() or
            search.lower() in content.lower()):
            title = snippet["title"]
            sid = snippet["id"]
            lb.insert(tk.END, f"{title} [{sid}]")
            visible_snippets.append(snippet)
            pass
        pass
    for elm in code_elems:
        update_highlight(elm)
        pass
    pass

def update_highlight(text):
    highlights = tk_find_all(text, sv.get())
    text.tag_configure("searchHighlight", background="#ffff00")
    text.tag_remove("searchHighlight", "1.0", tk.END)
    for highlight in highlights:
        text.tag_add("searchHighlight", highlight[0], highlight[1])
        pass
    pass

def save_snippet_file(snippet, fname, content):
    local_gitlab_url, gitlab_headers = premade_request()
    payload = {
        "files": [
            {
                "action": "update",
                "file_path": fname,
                "content": content,
            }
        ],
    }
    resp_raw = requests.put(local_gitlab_url + f"snippets/{snippet['id']}",
                            json=payload, headers=gitlab_headers)
    resp = resp_raw.json()
    if "id" not in resp:
        print(f"error, can't update snippet: {resp}")
        return
    snippet['content'][fname] = content
    dump_snippets()
    pass

def delete_snippet_file(snippet, fname):
    local_gitlab_url, gitlab_headers = premade_request()
    payload = {
        "files": [
            {
                "action": "delete",
                "file_path": fname,
            }
        ],
    }
    resp_raw = requests.put(local_gitlab_url + f"snippets/{snippet['id']}",
                            json=payload, headers=gitlab_headers)
    resp = resp_raw.json()
    if "id" not in resp:
        print(f"error, can't delete snippet: {resp}")
        return
    del snippet['content'][fname]
    dump_snippets()
    on_select(None, True)
    pass

# It would be 1'000'000 times better if {"action": "update"} also created
# new snippets, but we live in ~~society~~ gitlab API and we need separate
# method for it.
def add_file_snippet(snippet, fname):
    if fname == "":
        print("error, can't create files with empty filenames")
        # Actually, I can, I just don't want to deal with Gitlab random
        # bullshit name generator (tm)
        return
    content = "new file"
    local_gitlab_url, gitlab_headers = premade_request()
    payload = {
        "files": [
            {
                "action": "create",
                "file_path": fname,
                "content": content,
            }
        ],
    }
    resp_raw = requests.put(local_gitlab_url + f"snippets/{snippet['id']}",
                            json=payload, headers=gitlab_headers)
    resp = resp_raw.json()
    if "id" not in resp:
        print(f"error, can't create snippet file: {resp}")
        return
    snippet['content'][fname] = content
    dump_snippets()
    on_select(None, True)
    pass

def delete_snippet(snippet):
    global snippets
    local_gitlab_url, gitlab_headers = premade_request()
    requests.delete(local_gitlab_url + f"snippets/{snippet['id']}", headers=gitlab_headers)
    for i, x in enumerate(snippets):
        if x["id"] == snippet["id"]:
            snippets.pop(i)
            break
        pass
    dump_snippets()
    update_search()
    pass

def create_snippet(title):
    global snippets
    if title == "":
        print("error, can't create snippet with empty filename")
        return
    local_gitlab_url, gitlab_headers = premade_request()
    payload = {
        "title": title,
        "visibility": "private",
        "files": [
            {
                "content": "New file",
                "file_path": "new_file.txt",
            }
        ]
    }
    resp_raw = requests.post(local_gitlab_url + "snippets", json=payload, headers=gitlab_headers)
    resp = resp_raw.json()
    if "id" not in resp:
        print(f"error, can't create file: {resp}")
        return
    sess = requests.Session()
    sess.headers = gitlab_headers
    update_snippet_content(resp, sess)
    snippets.append(resp)
    dump_snippets()
    update_search()
    pass

last_selected_element = None

def on_select(event=None, use_last_selection=False):
    global code_elems, last_selected_element
    sel = lb.curselection()
    if use_last_selection:
        sel = last_selected_element
    if sel:
        last_selected_element = sel
        search = sv.get().lower()
        code_elems = list()
        for tab in tabs.tabs():
            tabs.forget(tab)
        ind = sel[0]
        snippet = visible_snippets[ind]
        title = snippet["title"]
        sid = snippet["id"]
        sn_name.set(f"{title} [{sid}]")
        sn_desc.set(snippet["description"])
        for fname, fcont in snippet["content"].items():
            search_in_file = search in fcont.lower()
            ftab = tk.Frame(tabs)
            fbtns = tk.Frame(ftab)
            new_file_name = tk.StringVar(fbtns, name=fname)
            new_file_entry = tk.Entry(fbtns, textvariable=new_file_name)
            tab_name = fname
            if search != "" and search_in_file:
                tab_name = "[[" + tab_name + "]]"
                pass
            tabs.add(ftab, text=tab_name)
            code_elem = tk.Text(ftab, height=0)
            code_elem.insert(tk.END, fcont)
            update_highlight(code_elem)
            code_elem.pack(expand=1, fill="both")
            save_btn = tk.Button(fbtns, text="Save file", command=
                                 lambda s=snippet, v=fname, c=code_elem: save_snippet_file(s, v, c.get("0.0", tk.END)))
            delete_btn = tk.Button(fbtns, text="Delete file", command=
                                   lambda s=snippet, v=fname: delete_snippet_file(s, v))
            new_file_btn = tk.Button(fbtns, text="New file", command=
                                     lambda s=snippet, v=new_file_name: add_file_snippet(s, v.get()))
            save_btn.pack(side='left')
            delete_btn.pack(side='left')
            new_file_btn.pack(side='left')
            new_file_entry.pack(side='left')
            fbtns.pack()
            code_elems.append(code_elem)
            pass
        pass
    pass

update_search()

window.config(menu=menubar)
window.geometry("800x600")
sv.trace("w", update_search)
lb.bind('<<ListboxSelect>>', on_select)
search_entry.pack(fill="x")
lb.pack(fill="x")
sn_name_label.pack()
sn_desc_label.pack()
tabs.pack(expand=1, fill="both")
snippets_btns.pack()
delete_snippet_btn.pack(side='left')
new_snippet_btn.pack(side='left')
new_snippet_entry.pack(side='left')

search_entry.focus()
window.mainloop()
