# TkSnippet

Гитлаб сниппеты без боли.

![Screenshot](./assets/screenshot.png)

## Установка

```shell
pip install -r requirements.txt
```

## Настройка

По умолчанию tksnippet не будет сохнарять ваши настройки и вам придётся
указывать их каждый раз, когда вы захотите обновить сниппеты. Но вы можете
установить их в переменные окружения: `GITLAB_TOKEN`, `GITLAB_PROJECT` and
`GITLAB_URL`.

## Запуск

Просто запустите `main.py`:

```shell
python3 ./main.py
```

Это создаст `snippets.json` со всеми вашими сниппетами (но без токена).
