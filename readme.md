# TkSnippet

[Russian version](./readme.ru.md)

Manage your gitlab snippets without pain.

![Screenshot](./assets/screenshot.png)

## Installing

```shell
pip install -r requirements.txt
```

## Configure

By default tksnippet won't save your preferences and you'll need to enter it
every time when you want to update snippets. But if you want to save it, you
can export env vars `GITLAB_TOKEN`, `GITLAB_PROJECT` and `GITLAB_URL`.

## Running

Just run `main.py`:

```shell
python3 ./main.py
```

It will create `snippets.json` with all your snippets content in there (but
there won't be your token).
